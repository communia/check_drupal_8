Version 1.1
-----------
* Clean up html and output all in plain text, to prevent breaking newlines in icingaweb output.
* Remove core errors as we follow a new strategy. Rather than allow obtaining separate error
and warnings filtering it on drush state, we can obtain everything and let choose automatically
the state of the chapter based on the presence of criticals and warnings.
* Various bugfixes.

Version 1
-----------
Work with drupal 8 and 9 and drush 10.3.6 and composer.

Version 0.8 (unreleased)
-----------


Version 0.7
-----------

- [Fea]		Be able to suppress locked modules (Thanks to https://github.com/fdellwing)


Version 0.6
-----------

- [Fea]		Set language for drush commands to English through forced config file


Version 0.5
-----------

- [Fea]		Drupal 8 support
- [Fix]		Fix core error detection
- [Fix]		Fix spelling


Version 0.4
-----------

- [Fea]		Implemented multisite checking via `-i`


Version 0.3
-----------

- [Fea]		Added performance data: Sec Updates
- [Fea]		Added performance data: Updates
- [Fea]		Added performance data: Core errors
- [Fea]		Added performance data: Core warnings
- [Fea]		Added performance data: DB migrations
- [Fea]		Improved short output


Version 0.2
-----------

- [Fea]		Implemented `check_drupal_log`
- [Fix]		Fixed misspelled variables
- [Enh]		First check all arguments, then run the checks
- [Enh]		Move scripts to bin directory
- [Enh]		Doc directory for more documentation


Version 0.1
-----------

- [Fea]		Check for Drupal security updates
- [Fea]		Check for Drupal system updates
- [Fea]		Check for Drupal required database updates
- [Fea]		Check for Drupal core errors
- [Fea]		Check for Drupal core warnings
- [Fea]		Every check can specify nagios severity (Error or Warning)
- [Fea]		Specify custom name for nagios short output
- [Fea]		Be able to successfully recognize valid Drupal6 or Drupal7 document root
- [Fea]		Detailed information in nagios long output
- [Fea]		Basic performance data fow: how many OKs, Errors, Warnings and Unknowns

